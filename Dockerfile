FROM postgres

WORKDIR /

RUN gpasswd -a postgres ssl-cert

COPY certs/dev-only-server.key /var/lib/postgresql/dev-only-server.key

COPY certs/dev-only-server.crt /var/lib/postgresql/dev-only-server.crt

RUN chown postgres:ssl-cert /var/lib/postgresql/dev-only-server.*

RUN chmod 600 /var/lib/postgresql/dev-only-server.*

USER postgres:ssl-cert
