build:
	docker build -t registry.gitlab.com/jstone28/postgres:latest -t registry.gitlab.com/jstone28/postgres:1.1 .

run:
	docker run -it registry.gitlab.com/jstone28/postgres:latest bash

push:
	docker push registry.gitlab.com/jstone28/postgres:1.1
	docker push registry.gitlab.com/jstone28/postgres:latest


